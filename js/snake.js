/*
Name of the project : Snake game using html and js
File name : snake.js
Description : Snake-game backend using js
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
*/


var canvas = document.querySelector( 'canvas ');
canvas.width = 440 ;
canvas.height = 440 ;
var context = canvas.getContext('2d');


var paused=false;
var speed=300;
var clr="white";
var inter;
var pos = 20;
var snakePos = [];
var score = 0;
var dir="assign";
let snakeX;
let snakeY;
let flag=0;


function pause() {
    if(paused) {
      paused=false;
    }else {
      paused=true;
    }
}


document.addEventListener( "keydown" , direction);


/*control for key*/
function direction( event) {
    if(event.keyCode == 37 && dir != "RIGHT" ) {
        dir = "LEFT";
    }else if ( event.keyCode == 38 && dir != "DOWN" ) {
        dir = "UP";
    }else if ( event.keyCode == 39 && dir != "LEFT" ) {
        dir = "RIGHT";
    }else if( event.keyCode == 40 && dir != "UP" ) {
        dir = "DOWN";
    }
    else if( event.keyCode == 32) {
        pause();
    }
}


/*update the snake position*/
function updatePosition(){
    if(dir!="assign") {
        switch (dir) {
            case "LEFT": snakeX-=pos;
                break;
            case "UP": snakeY-=pos;
                break;
            case "RIGHT": snakeX+=pos;
                break;
            case "DOWN": snakeY+=pos;
                break;
        }
        if ( flag != 1 ) {
            snakePos.pop();
        }else {
            flag = 0;
        }
        gameOver();
        let newhead={ posX : snakeX,
            posY : snakeY };
        snakePos.unshift( newhead );
    }else {
        return;
    }
}


/*snake inetial position*/
snakePos[0] = {
    posX : 40,
    posY : 40
};
snakePos[1] = {
    posX : 20,
    posY : 40
};


/*fruit inetial position*/
var fruitPos = {
    posX : Math.floor(Math.random() * 20 + 1)*pos,
    posY : Math.floor(Math.random() * 20 + 1)*pos
};


function checkCollision(){
    for( let i=snakePos.length-1; i >0 ; i--){
        if( snakeX == snakePos[i].posX && snakeY == snakePos[i].posY){
            return true;
        }
    }
    return false;
}


function gameOver(){
    if(snakeX<0 || snakeX>21*pos || snakeY < 0 || snakeY>21*pos ||
    checkCollision() ) {
        clearInterval(inter);
    }
  }


/*draw the snake and fruit*/
function draw() {
    if(paused) {
    }else {
        context.clearRect(0,0,canvas.width,canvas.height);
        context.fillStyle = clr;
        context.fillRect(0,0,canvas.width,canvas.height);
        context.fillStyle = 'red';
        context.fillRect(fruitPos.posX, fruitPos.posY,
        pos, pos);
        for(let x = 0; x < snakePos.length; x++ ){
            context.fillStyle = x==0?'black':'blue';
            context.fillRect(snakePos[x].posX, snakePos[x].posY,
            pos, pos);
        }
        document.getElementById("score").innerHTML = score;
        snakeX = snakePos[0].posX;
        snakeY = snakePos[0].posY;
        if(snakeX==fruitPos.posX && snakeY==fruitPos.posY) {
            score++;
            flag=1;
            fruitPos.posX = Math.floor(Math.random() * 20 + 1)*pos;
            fruitPos.posY = Math.floor(Math.random() * 20 + 1)*pos;
        }
        updatePosition();
    }
 }


draw();


document.getElementById('start').addEventListener('click',function() {
    if(document.getElementById("level").value == "easy"){
        speed = 300;
    }else if(document.getElementById('level').value == "medium") {
        speed = 250;
    }else if(document.getElementById('level').value == "hard") {
        speed = 200;
    }
    if(document.getElementById("color").value == "yellow") {
        clr = "yellow";
    }else if(document.getElementById('color').value=="white") {
        clr="white";
    }  else if (document.getElementById('color').value=="orange") {
        clr="orange";
    }
       inter = setInterval(draw, speed);
})


document.getElementById('restart').addEventListener('click',function(){
    window.location.reload();
 })
